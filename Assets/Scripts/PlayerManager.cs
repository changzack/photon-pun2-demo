﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.zackspace
{
    public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        #region Static Fields
        
        [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
        public static GameObject LocalPlayerInstance;
        
        #endregion
        
        #region Public Fields

        [Tooltip("The current Health of our player")]
        public float health = 1f;

        [Tooltip("The Player's UI GameObject Prefab")] [SerializeField]
        private GameObject playerUiPrefab;

        #endregion
        
        #region Private Fields

        [Tooltip("The Beams GameObject to control")] [SerializeField]
        private GameObject beams;
        private bool isFiring;
        
        #endregion

        #region MonoBehaviour Callbacks

        private void Awake()
        {
            if (beams == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> Beams Reference.", this);
            }
            else
            {
                beams.SetActive(false);
            }

            // #Important
            // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
            if (photonView.IsMine)
            {
                LocalPlayerInstance = gameObject;
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            CameraWork _cameraWork = gameObject.GetComponent<CameraWork>();
            if (_cameraWork != null)
            {
                if (photonView.IsMine)
                {
                    _cameraWork.OnStartFollowing();
                }
            }
            else
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork component on playerPrefab.", this);
            }

            SceneManager.sceneLoaded += OnSceneLoaded;

            CreatePlayerUi();
        }


        // Update is called once per frame
        void Update()
        {
            if (photonView.IsMine || !PhotonNetwork.IsConnected)
            {
                ProcessInputs();
                
                if (health <= 0)
                {
                    GameManager.Instance.LeaveRoom();
                }
            }
            
            // Trigger Beams active state
            if (beams != null && isFiring != beams.activeInHierarchy)
            {
                beams.SetActive(isFiring);
            }
        }

        /// <summary>
        /// MonoBehaviour method called when the Collider 'other' enters the trigger.
        /// Affect health of the Player if the collider is a beam
        /// Note: When jumping and firing at the same, you'll find that the player's own beam intersects with itself
        /// One could move the collider further away to prevent this or check if the beam belongs to the player.
        /// </summary>
        /// <param name="other">Other.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (!photonView.IsMine)
            {
                return;
            }

            if (!other.name.Contains("Beam"))
            {
                return;
            }

            health -= 0.1f;
        }

        /// <summary>
        /// MonoBehavior method called once per frame for every Collider 'other' that is touching the trigger.
        /// We're gonig to affect the health while the beams are touching the player
        /// </summary>
        /// <param name="other">Other.</param>
        private void OnTriggerStay(Collider other)
        {
            if (!photonView.IsMine)
            {
                return;
            }

            if (!other.name.Contains("Beam"))
            {
                return;
            }

            health -= 0.1f * Time.deltaTime;
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        #endregion

        #region Custom

        /// <summary>
        /// Processes the inputs. Maintain a flat representing when the user is pressing Fire.
        /// </summary>
        private void ProcessInputs()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!isFiring)
                {
                    isFiring = true;
                }
            }

            if (Input.GetButtonUp("Fire1"))
            {
                if (isFiring)
                {
                    isFiring = false;
                }
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadingMode)
        {
            CalledOnLevelWasLoaded(scene.buildIndex);
        }

        private void CalledOnLevelWasLoaded(int level)
        {
            // Check if we are outside the Arena and if it's the case, spawn around the center of the arena in a safe zone
            if (!Physics.Raycast(transform.position, -Vector3.up, 5f))
            {
                transform.position = new Vector3(0f, 5f, 0f);
            }

            CreatePlayerUi();
        }

        private void CreatePlayerUi()
        {
            if (playerUiPrefab != null)
            {
                GameObject _uiGo = Instantiate(playerUiPrefab);
                _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
            }
            else
            {
                Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
            }
        }

        #endregion

        #region IPunObservable Implmentation

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(isFiring);
                stream.SendNext(health);
            }
            else
            {
                // Network player, receive data
                isFiring = (bool) stream.ReceiveNext();
                health = (float) stream.ReceiveNext();
            }
        }

        #endregion
    }
}


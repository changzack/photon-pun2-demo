﻿using UnityEngine;

namespace com.zackspace
{
    public class CameraWork : MonoBehaviour
    {
        #region Private Fields

        [Tooltip("The distance in the local x-z plane to the target")] 
        [SerializeField]
        private float distance = 7.0f;
        
        [Tooltip("The height we want the camera to be above the target")]
        [SerializeField]
        private float height = 3.0f;

        [Tooltip("The Smooth time lag for the height of the camera")] 
        [SerializeField]
        private float heightSmoothLag = 0.3f;

        [Tooltip("Allow the camera to be offsetted vertically from the target, for example giving more view of the scenery and less ground")] 
        [SerializeField]
        private Vector3 centerOffset = Vector3.zero;
        
        [Tooltip("Set this as false if a component of a prefab being instanciated by Photon Network, and manually call OnStartFollowing() when and if needed.")] 
        [SerializeField]
        private bool followOnStart = false;

        // Cached transform of the target
        private Transform cameraTransform;
        
        // Maintain a flag internally to reconnect if target is lost or camera is switched
        private bool isFollowing;
        
        // Represents the current velocity, this value is modified by SmoothDamp() every time you call it
        private float heightVelocity;

        // Represents the position we are trying to reach using SmoothDamp()
        private float targetHeight = 100000.0f;

        #endregion

        #region MonoBehaviour Callbacks

        // Start is called before the first frame update
        private void Start()
        {
            if (followOnStart)
            {
                OnStartFollowing();
            }
        }

        // Update is called once per frame
        private void LateUpdate()
        {
            // The transform target may not destroy on level load,
            // so we need to cover corner cases where the Main Camera is different every time we load a new scene, and reconnect when that happens
            if (cameraTransform == null && isFollowing)
            {
                OnStartFollowing();
            }
            
            // Only follow is explicitly declared
            if (isFollowing)
            {
                Apply();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Raises the start following event.
        /// Use his when you don't know a the time of editing what to follow, typically instances managed by the photon network.
        /// </summary>
        public void OnStartFollowing()
        {
            cameraTransform = Camera.main?.transform;
            isFollowing = true;
            // We don't smooth anything, we go straight to the right camera shot
            Cut();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Follow the target smoothly
        /// </summary>
        private void Apply()
        {
            // Target center
            Vector3 targetCenter = transform.position + centerOffset;

            // Damp the height
            float camCurrentHeight = cameraTransform.position.y;
            targetHeight = targetCenter.y + height;
            camCurrentHeight = Mathf.SmoothDamp(camCurrentHeight, targetHeight, ref heightVelocity, heightSmoothLag);
            
            // Convert the angle into rotation, by which we then reposition the camera
            float targetAngleY = transform.eulerAngles.y;
            Quaternion targetRotationY = Quaternion.Euler(0, targetAngleY, 0);
            
            // Set the position of the camera on the x-z plane to:
            // distance meters behind the target
            cameraTransform.position = targetCenter;
            cameraTransform.position += targetRotationY * Vector3.back * distance;
            
            // Set the height of the camera (y plane)
            cameraTransform.position = new Vector3(cameraTransform.position.x, camCurrentHeight, cameraTransform.position.z);
            
            // Always look at the target
            SetUpRotation(targetCenter);
        }

        /// <summary>
        /// Directly position the camera to the specified Target and center
        /// </summary>
        private void Cut()
        {
            float oldHeightSmooth = heightSmoothLag;
            heightSmoothLag = 0.001f;
            Apply();
            heightSmoothLag = oldHeightSmooth;
        }

        /// <summary>
        /// Sets up the rotation of the camera to always be behind the target
        /// </summary>
        /// <param name="centerPos">Center position.</param>
        private void SetUpRotation(Vector3 centerPos)
        {
            Vector3 cameraPos = cameraTransform.position;
            Vector3 offsetToCenter = centerPos - cameraPos;
            
            // Generate base rotation only around y-axis
            Quaternion yRotation = Quaternion.LookRotation(new Vector3(offsetToCenter.x, 0, offsetToCenter.z));
            Vector3 relativeOffset = Vector3.forward * distance + Vector3.down * height;
            cameraTransform.rotation = yRotation * Quaternion.LookRotation(relativeOffset);
        }

        #endregion
        
    }
}


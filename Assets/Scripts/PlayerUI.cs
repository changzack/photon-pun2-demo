﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace com.zackspace
{
    public class PlayerUI : MonoBehaviour
    {
        #region Private Fields

        [Tooltip("UI Text to dispaly Player's Name")] [SerializeField]
        private Text playerNameText;

        [Tooltip("UI Slider to display Player's Health")] [SerializeField]
        private Slider PlayerHealthSlider;
        
        [Tooltip("Pixel offset from the player target")] [SerializeField]
        private Vector3 screenOffset = new Vector3(0f, 30f, 0f);

        private PlayerManager target;
        private float characterControllerHeight = 0f;
        private Transform targetTransform;
        private Renderer targetRenderer;
        private CanvasGroup _canvasGroup;
        private Vector3 targetPosition;

        #endregion

        #region MonoBehaviors

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);
        }

        // Update is called once per frame
        void Update()
        {
            // Reflect the Player Health
            if (PlayerHealthSlider != null)
            {
                PlayerHealthSlider.value = target.health;
            }

            // Destroy itself if the target is null. it's a fail safe when Photon is destroying Instances of a Player over the network.
            if (target == null)
            {
                Destroy(gameObject);
                return;
            }
        }

        private void LateUpdate()
        {
            // Do not show the UI if we are not visible to the camera, thus avoid potential bugs with seeing the UI, but not the player itself.
            if (targetRenderer != null)
            {
                _canvasGroup.alpha = targetRenderer.isVisible ? 1f : 0f;
            }
            
            // #Critical
            // Follow the Target GameObject on screen
            if (targetTransform != null)
            {
                targetPosition = targetTransform.position;
                targetPosition.y += characterControllerHeight;
                transform.position = Camera.main.WorldToScreenPoint(targetPosition) + screenOffset;
            }
        }

        #endregion

        #region Public Methods

        public void SetTarget(PlayerManager _target)
        {
            if (_target == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> PlayerMakerManager target for PlayerUI.SetTarget.", this);
                return;
            }
        
            // Cache references for efficiency
            target = _target;
            if (playerNameText != null)
            {
                playerNameText.text = target.photonView.Owner.NickName;
            }
            
            // Make UI follow the player
            targetTransform = target.GetComponent<Transform>();
            targetRenderer = target.GetComponent<Renderer>();
            CharacterController characterController = target.GetComponent<CharacterController>();
            if (characterController != null)
            {
                characterControllerHeight = characterController.height;
            }
        }

        #endregion
    }
}

